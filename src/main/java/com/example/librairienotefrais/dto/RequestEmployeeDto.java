package com.example.librairienotefrais.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestEmployeeDto implements EmployeeDto {
    private String lastName;
    private String firstName;

    private String password;
}
