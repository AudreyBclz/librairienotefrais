package com.example.librairienotefrais.dto;

import com.example.librairienotefrais.enums.Status;
import com.example.librairienotefrais.model.Category;
import com.example.librairienotefrais.model.Proof;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseExpenseReportDto implements ExpenseReportDto {
    private int id;
    private String title;
    private String description;
    private Double amount;
    private List<Proof> proofs;
    private Date date;
    private Status status;
    private int admin;
    private int employee;
    private Category category;

    @Override
    public String toString() {
        return "ResponseExpenseReportDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", status=" + status +
                ", admin=" + admin +
                ", employee=" + employee +
                ", category=" + category +
                '}';
    }
}
