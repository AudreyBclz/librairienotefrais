package com.example.librairienotefrais.dto;

import com.example.librairienotefrais.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestExpenseReportDto implements ExpenseReportDto {
    private String title;
    private String description;
    private double amount;
    private List<MultipartFile> proofs;
    private Date date;
    private Status status;
    private int admin;
    private int employee;
    private int category;
}
