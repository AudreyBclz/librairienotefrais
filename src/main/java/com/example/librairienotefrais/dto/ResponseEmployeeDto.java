package com.example.librairienotefrais.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEmployeeDto implements EmployeeDto{
    private int id;

    private String lastName;
    private String firstName;

    private String password;

    private int authorities;
}
