package com.example.librairienotefrais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Category {
    private int id;
    private String name;
    @JsonIgnore
    private List<ExpenseReport> expenseReports;

    public Category(String name, List<ExpenseReport> expenseReports) {
        this.name = name;
        this.expenseReports = expenseReports;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
        }
    }
