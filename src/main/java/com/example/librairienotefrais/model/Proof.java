package com.example.librairienotefrais.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Proof {

    private int id;
    private String path;
    private ExpenseReport expenseReport;

    public Proof(String path, ExpenseReport expenseReport) {
        this.path = path;
        this.expenseReport = expenseReport;
    }

    @Override
    public String toString() {
        return "Proof{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", expenseReport=" + expenseReport +
                '}';
    }
}
